#include "mbed.h"
#include "USBSerial.h"

#include "USBKeyboard.h"
#include "utilscrc16.h"

#define SLIP_END             0xC0    /* indicates end of packet */
#define SLIP_ESC             0xDB    /* indicates byte stuffing */
#define SLIP_ESC_END         0xDC    /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC         0xDD    /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON  0xDE /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF 0xDF /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON 0x11 /* indicates XON charater */
#define XOFF 0x13 /* indicates XOFF charater */
#define RECEIVE_SIZE 64




DigitalOut _ledUart(P0_11);
DigitalOut _ledHid(P0_21);
DigitalOut _ledMsd(P0_20);
Serial _serial(P0_19, P0_18);
USBKeyboard _keyboard;
uint8_t _receiving[RECEIVE_SIZE];
event_callback_t    serialEventCb;

// USBSerial serial;


int decode(const uint8_t *packet,uint16_t &size) ;

// This function is called when a character goes into the TX buffer.
void txCallback() {
        _keyboard._putc('B');

        _ledUart = !_ledUart;
        _ledHid = !_ledHid;
        _ledMsd = !_ledMsd;
}

// This function is called when a character goes into the RX buffer.
void rxCallback() {

    static int index = 0;
    char c;
    c = _serial.getc();
    _receiving[index] = c;

//    serial.printf("%x ", _receiving[index]);


    index ++;
    if(c == 0xC0){
        index = 0;
//        serial.printf("\r\nouttttttttttttttttttt\r\n");
        _keyboard.printf("00%d%d%d\r\n", _receiving[1],_receiving[2],_receiving[3]);
    }
//     _serial.putc(_serial.getc());

}



int main() {

    _serial.baud(115200);
    _serial.attach(&txCallback, Serial::TxIrq);
    _serial.attach(&rxCallback, Serial::RxIrq);

    _ledUart = 0;
    _ledHid = 0;
    _ledMsd = 0;


    while(1){

//        _keyboard.mediaControl(KEY_VOLUME_DOWN);
//        _keyboard.printf("Hello World from Mbed \r\n");
//        _keyboard.keyCode('s', KEY_CTRL);
//        _keyboard.keyCode(KEY_CAPS_LOCK);
//        _keyboard.mediaControl(KEY_VOLUME_DOWN);
//        _keyboard.keyCode(KEY_CAPS_LOCK);
//        _keyboard._putc('A');
        _ledHid = !_ledHid;
//        serial.printf("I am a virtual serial port\r\n");
        wait(1);
     }
}


